package org.lawlsec.UHCKit.Configuration;

import org.lawlsec.UHCKit.UHCKit;

import java.util.Optional;

public class MainConfig {

    ///region Classes

    public static class DatabaseConfig {
        private String User;
        private String Pass;
        private String Host;
        private String Port;
        private String Database;
        private boolean SSL;

        public String getUser() {
            return User;
        }

        public void setUser(String user) {
            User = user;
        }

        public String getPass() {
            return Pass;
        }

        public void setPass(String pass) {
            Pass = pass;
        }

        public String getHost() {
            return Host;
        }

        public void setHost(String host) {
            Host = host;
        }

        public String getPort() {
            return Port;
        }

        public void setPort(String port) {
            Port = port;
        }

        public String getDatabase() {
            return Database;
        }

        public void setDatabase(String database) {
            Database = database;
        }

        public boolean isSSL() {
            return SSL;
        }

        public void setSSL(boolean SSL) {
            this.SSL = SSL;
        }

        public DatabaseConfig(String user, String pass, String host, String port, String database, boolean ssl) {
            User = user;
            Pass = pass;
            Host = host;
            Port = port;
            Database = database;
            SSL = ssl;
        }
    }

    ///endregion

    ///region Database

    public static String getUser() {
        return UHCKit.Get().getConfig().getString("Database.User");
    }

    public static String getPass() {
        return UHCKit.Get().getConfig().getString("Database.Pass");
    }

    public static String getHost() {
        return UHCKit.Get().getConfig().getString("Database.Host");
    }

    public static String getPort() {
        return UHCKit.Get().getConfig().getString("Database.Port");
    }

    public static String getDatabase() {
        return UHCKit.Get().getConfig().getString("Database.Database");
    }

    public static boolean isSSL() {
        return UHCKit.Get().getConfig().getBoolean("Database.SSL");
    }

    public static Optional<DatabaseConfig> getDatabaseConfig() {
        if ((getHost() == null) ||
            (getPort() == null) ||
            (getDatabase() == null) ||
            (getUser() == null) ||
            (getPass() == null)) {

            return Optional.empty();
        }

        return Optional.of(
            new DatabaseConfig(
                getUser(),
                getPass(),
                getHost(),
                getPort(),
                getDatabase(),
                isSSL()
        ));
    }

    ///endregion

    public enum Difficulty {
        HARD,
        ULTRA
    }

    public static String getWorld() {
        return UHCKit.Get().getConfig().getString("Server.World");
    }

    public static Difficulty getDifficulty() {
        switch (UHCKit.Get().getConfig().getString("Global.Difficulty", "hard").toLowerCase()) {

            case "hard":
                return Difficulty.HARD;

            case "ultra":
                return Difficulty.ULTRA;

            default:
                return Difficulty.HARD;

        }
    }

    public static int getMaxMindUserID() {
        return UHCKit.Get().getConfig().getInt("MaxMind.UserID");
    }

    public static String getMaxMindLicense() {
        return UHCKit.Get().getConfig().getString("MaxMind.License");
    }
}
