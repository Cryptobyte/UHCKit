package org.lawlsec.UHCKit.Configuration;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.lawlsec.UHCKit.UHCKit;

import java.io.File;
import java.time.Period;

public class TournamentConfig {
    private static String Config = "tournaments.yml";

    private static File getDataFolder() {
        return UHCKit.Get().getDataFolder();
    }

    private static File getFile() {
        return new File(getDataFolder(), Config);
    }

    private static FileConfiguration getConfig() {
        return YamlConfiguration.loadConfiguration(getFile());
    }

    public static Period getTournamentTime() {
        return Period.parse(
            getConfig().getString("Global.Length", "3d")
        );
    }

    public static int getPlayerLimit() {
        return getConfig().getInt("Global.Players.Limit", 30);
    }

    public static boolean getPlayerGeoGrouping() {
        if ((MainConfig.getMaxMindLicense() == null) ||
            (!MainConfig.getMaxMindLicense().equals(""))) {

            return false;
        }

        return getConfig().getBoolean("Global.Players.GeoGrouping", true);
    }

    public static boolean getPlayerOpenRegistration() {
        return getConfig().getBoolean("Global.Players.OpenRegistration", true);
    }

    public static void saveDefaultConfig() {
        if (!getFile().exists()) {
            UHCKit.Get().saveResource(Config, false);
        }
    }
}
