package org.lawlsec.UHCKit.Persistence;

import org.lawlsec.UHCKit.Configuration.MainConfig;
import org.lawlsec.UHCKit.UHCKit;
import org.lawlsec.bugger.Enums.DebugLevel;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Optional;

public class Database {
    /**
     * Get a connection to the database.
     * It is your job to close this connection!
     *
     * @return Connection to Database
     */
    public static Optional<Connection> getConnection() {
        Optional<MainConfig.DatabaseConfig> Config = MainConfig.getDatabaseConfig();

        if (!Config.isPresent()) {
            UHCKit.GetDebugger().Log(DebugLevel.Critical,
                "Database info missing from config!"
            );

            return Optional.empty();
        }

        try {
            Class.forName("com.mysql.jdbc.Driver");

            return Optional.of(DriverManager.getConnection(
                    String.format(
                            "jdbc:mysql://%s:%s/%s?useSSL=%s",
                            Config.get().getHost(),
                            Config.get().getPort(),
                            Config.get().getDatabase(),
                            String.valueOf(Config.get().isSSL()).toLowerCase()
                    ),
                    Config.get().getUser(),
                    Config.get().getPass()
            ));

        } catch (SQLException e) {
            UHCKit.GetDebugger().Log(DebugLevel.Error,
                    e.getMessage()
            );

            return Optional.empty();

        } catch (ClassNotFoundException e) {
            UHCKit.GetDebugger().Log(DebugLevel.Critical,
                    "jdbc driver not found!"
            );

            return Optional.empty();
        }
    }
}
