package org.lawlsec.UHCKit.Utilities;

import com.maxmind.geoip2.WebServiceClient;
import com.maxmind.geoip2.exception.GeoIp2Exception;
import org.lawlsec.UHCKit.Configuration.MainConfig;

import java.io.IOException;
import java.net.InetAddress;
import java.util.Optional;

public class General {
    public static Optional<String> getTimezone(String Addr) {
        try {

            WebServiceClient _Client =  new WebServiceClient.Builder(
                MainConfig.getMaxMindUserID(),
                MainConfig.getMaxMindLicense()

            ).build();

            return Optional.of(_Client.insights(InetAddress.getByName(Addr))
                .getLocation()
                .getTimeZone());

        } catch(IOException | GeoIp2Exception e) { return Optional.empty(); }
    }
}
