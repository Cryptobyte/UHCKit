package org.lawlsec.UHCKit.Utilities.Const;

import net.md_5.bungee.api.ChatColor;

public class MessageColors {
    public static ChatColor COLOR_BASE = ChatColor.WHITE;
    public static ChatColor COLOR_KILL = ChatColor.GREEN;
    public static ChatColor COLOR_DEATH = ChatColor.RED;
    public static ChatColor COLOR_ITEM = ChatColor.GOLD;
}
