package org.lawlsec.UHCKit.Listeners;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.lawlsec.UHCKit.Configuration.MainConfig;
import org.lawlsec.UHCKit.Utilities.Const.MessageColors;

public class InternalListeners implements Listener {
    @EventHandler
    public void onDeath(PlayerDeathEvent e) {
        if (MainConfig.getDifficulty() == MainConfig.Difficulty.ULTRA) {
            e.setDeathMessage("");
            return;
        }

        if (e.getEntity().getKiller() instanceof Player) {
            Player _Player = e.getEntity();
            Player _Killer = e.getEntity().getKiller();

            StringBuilder _Builder = new StringBuilder();

            _Builder.append(MessageColors.COLOR_KILL).append(_Player.getName());
            _Builder.append(MessageColors.COLOR_BASE).append(" killed ");
            _Builder.append(MessageColors.COLOR_DEATH).append(_Killer.getName());

            if (_Killer.getMainHand() != null) {
                _Builder.append(MessageColors.COLOR_BASE).append(" with ");
                _Builder.append(MessageColors.COLOR_ITEM).append(_Killer.getMainHand().name());
            }

            e.setDeathMessage(_Builder.toString());
        }
    }
}
