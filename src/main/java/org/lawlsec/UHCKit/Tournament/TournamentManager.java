package org.lawlsec.UHCKit.Tournament;

import org.bukkit.scheduler.BukkitRunnable;
import org.lawlsec.UHCKit.Persistence.Database;
import org.lawlsec.UHCKit.UHCKit;
import org.lawlsec.bugger.Enums.DebugLevel;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Optional;

public class TournamentManager {
    public TournamentManager() {
        new BukkitRunnable() {
            public void run() {
                try {
                    Optional<Connection> Connect = Database.getConnection();

                    if (!Connect.isPresent()) {
                        UHCKit.GetDebugger().Log(DebugLevel.Error,
                                "Tournament Manager Cannot Start: Can't Access Database Connection"
                        );

                        return;
                    }

                    Connection Conn = Connect.get();

                    Conn.prepareStatement(
                            ""

                    ).executeUpdate();

                    if (!Conn.isClosed())
                        Conn.close();

                } catch (SQLException e) {
                    UHCKit.GetDebugger().Log(DebugLevel.Critical, "Unable to initialize Database");
                    e.printStackTrace();
                }
            }

        }.runTaskAsynchronously(UHCKit.Get());
    }
}
